local cmd = vim.cmd
local g = vim.g
local opt = vim.opt

local Plug = vim.fn['plug#']

--
-- plugin registry
--

vim.call('plug#begin', '~/.config/nvim/plugged')

-- status line
Plug 'hoob3rt/lualine.nvim'
Plug 'kyazdani42/nvim-web-devicons'

-- color theme
Plug 'morhetz/gruvbox'

-- directory tree
Plug('scrooloose/nerdtree', {on = 'NERDTreeToggle'})

-- fuzzy finder
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

-- language server
Plug 'neovim/nvim-lspconfig'

-- enable more features of rust-analyzer
Plug 'simrat39/rust-tools.nvim'

-- syntax highlighting
Plug('nvim-treesitter/nvim-treesitter', {run = ':TSUpdate'})
Plug('cespare/vim-toml', {branch = 'main'})

-- autocompletion
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-vsnip'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-buffer'
Plug 'windwp/nvim-autopairs'

-- dope snippets
Plug 'hrsh7th/vim-vsnip'

vim.call('plug#end')

--
-- configuration
--

-- status line
require('lualine').setup()
options = {theme = 'gruvbox'}
require('nvim-web-devicons').setup()

-- language server support

-- python support
require('lspconfig').pyright.setup{}

-- rust support
-- Configure LSP through rust-tools.nvim plugin.
-- rust-tools will configure and enable certain LSP features for us.
-- See https://github.com/simrat39/rust-tools.nvim#configuration
local opts = {
    tools = { -- rust-tools options
        autoSetHints = true,
        inlay_hints = {
            show_parameter_hints = false,
            parameter_hints_prefix = "",
            other_hints_prefix = "",
        },
    },

    -- all the opts to send to nvim-lspconfig
    -- these override the defaults set by rust-tools.nvim
    -- see https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#rust_analyzer
    server = {
        -- on_attach is a callback called when the language server attachs to the buffer
        -- on_attach = on_attach,
        settings = {
            -- to enable rust-analyzer settings visit:
            -- https://github.com/rust-analyzer/rust-analyzer/blob/master/docs/user/generated_config.adoc
            ["rust-analyzer"] = {
                -- enable clippy on save
                checkOnSave = {
                    command = "clippy"
                },
            }
        }
    },
}
require('rust-tools').setup(opts)

-- autocompletion

-- set up completion
-- See https://github.com/hrsh7th/nvim-cmp#basic-configuration
local cmp = require'cmp'
cmp.setup({
  -- Enable LSP snippets
  snippet = {
    expand = function(args)
        vim.fn["vsnip#anonymous"](args.body)
    end,
  },
  mapping = {
    ['<C-p>'] = cmp.mapping.select_prev_item(),
    ['<C-n>'] = cmp.mapping.select_next_item(),
    -- Add tab support
    ['<S-Tab>'] = cmp.mapping.select_prev_item(),
    ['<Tab>'] = cmp.mapping.select_next_item(),
    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.close(),
    ['<CR>'] = cmp.mapping.confirm({
      behavior = cmp.ConfirmBehavior.Insert,
      select = true,
    })
  },

  -- Installed sources
  sources = {
    { name = 'nvim_lsp' },
    { name = 'vsnip' },
    { name = 'path' },
    { name = 'buffer' },
  },
})

require('nvim-autopairs').setup{}

-- telescope hotkeys
cmd[[nnoremap <leader>ff <cmd>Telescope find_files<cr>]]
cmd[[nnoremap <leader>fg <cmd>Telescope live_grep<cr>]]
cmd[[nnoremap <leader>fb <cmd>Telescope buffers<cr>]]
cmd[[nnoremap <leader>fh <cmd>Telescope help_tags<cr>]]

-- color theme
cmd[[colorscheme gruvbox]]

-- indent settings
require'nvim-treesitter.configs'.setup {
  indent = {
    enable = false,
	disable = {"python", },
  }
}

opt.tabstop = 4
opt.shiftwidth = 4

-- word wrap paragraphs
--opt.textwidth = 90

-- disable python PEP8
--g.python_recommended_style = 0

-- allow copy/paste with highlighting
opt.mouse = ""

-- window settings
--opt.number = true          -- show line number
opt.showmatch = true       -- highlight matching parenthesis
opt.splitright = true      -- vertical split to the right
opt.splitbelow = true      -- horizontal split to the bottom

-- Set completeopt to have a better completion experience
-- :help completeopt
-- menuone: popup even when there's only one match
-- noinsert: Do not insert text until a selection is made
-- noselect: Do not select, force user to select one from the menu
opt.completeopt = 'menuone,noinsert,noselect'

-- Avoid showing extra messages when using completion
opt.shortmess:append('c')
