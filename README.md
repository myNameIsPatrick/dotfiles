# dotfiles

A set of configuration files.

## Installation


First, clone the repo onto your workstation:

```
git clone git@gitlab.com:myNameIsPatrick/dotfiles.git ~/.dotfiles
```


Then, install `stow` via your preferred package manager, e.g.

```
dnf install stow
```

Finally, install the dotfiles for each program:

```
stow nvim
```
