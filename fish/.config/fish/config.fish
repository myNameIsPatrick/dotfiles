if status is-interactive
    # Commands to run in interactive sessions can go here
end

# turn off greeting
set fish_greeting

# cargo support
fish_add_path $HOME/.cargo/bin

# local binaries
fish_add_path $HOME/.local/bin

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
#eval /usr/bin/conda "shell.fish" "hook" $argv | source
# <<< conda initialize <<<
source /etc/fish/conf.d/conda.fish

# shell prompt
starship init fish | source
