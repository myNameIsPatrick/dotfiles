############################################################
## Bash common shell startup script
## by jrollins 
############################################################
## login: .bash{,_profile} (ssh)
## interactive: .bashrc (xterm,screen)

#umask 022

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

############################################################
## ENVIRONMENT VARIABLES ###################################

# set path
#export PATH=~/bin:$PATH:/usr/local/sbin:/usr/sbin:/sbin
#export PATH=$PATH:/usr/local/sbin:/usr/sbin:/sbin

export HISTFILESIZE=10000
# don't put duplicate lines in the history. See bash(1) for more options
export HISTCONTROL=ignoredups:erasedups
# append to the history file, instead of writing over
shopt -s histappend

# ignore EOF
export IGNOREEOF=

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(lesspipe)"
export LESSOPEN="| /usr/share/source-highlight/src-hilite-lesspipe.sh %s"
#export LESS="-R -i -P %l of %L"
export LESS="-R -X -F -i -P %l of %L"

# git options
export GIT_PAGER="less -FRSX"

############################################################
## COLOR TERM ##############################################

# function to test for color terminal
# this is used by .bash_prompt
function _is_color_term {
    # compliant with Ecma-48 (ISO/IEC-6429). (Lack of such support is
    # extremely rare, and such a case would tend to support setf
    # rather than setaf.)
    [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null
}

############################################################
## ALIASES #################################################

# enable color support of ls and also add handy aliases
if _is_color_term && which dircolors > /dev/null ; then
    eval "$(dircolors -b)"
    alias ls='ls --color=auto'
fi
alias la='ls -A'
alias ll='ls -lh'
alias lal='ls -alh'

alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -i'

alias grep='grep --color=auto'

############################################################
## SOURCE FILES ############################################

## source local startup configuration options
if [ -f ~/.bash_local ]; then
    source ~/.bash_local
fi

## fancy prompt
if [ -f ~/.bash_prompt ]; then
    source ~/.bash_prompt
fi

## source bash functions
if [ -f ~/.bash_functions ]; then
    source ~/.bash_functions
fi
